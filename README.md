# TDD Demo para novatos - Santiago Devops and Continuous Delivery #

TDD Demo

### Que necesito? ###

* [Java JDK 7 (Es posible usar JDK 6)](http://www.oracle.com/technetwork/es/java/javase/downloads/jdk7-downloads-1880260.html)
* [Eclipse (de preferencia Luna)](http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/luna/SR2/eclipse-jee-luna-SR2-macosx-cocoa.tar.gz) 
* [Apache Maven](https://maven.apache.org/)
* Infinitest Plugin para Eclipse (opcional)

### Para calentar ###
Este sencillo ejercicio permite ver en acción la disciplina de TDD, generando tests en primer lugar (TFD), luego haciendo pasar los tests para luego refactorizar. El uso de Infinitest nos permitirá evaluar nuestra suite cada vez que se guarde un nuevo cambio.
Para comenzar:

* Importar proyecto a IDE Eclipse como proyecto Maven
* Generar clase CalculadoraTest.java dentro de src/test/java en package tdd.demo
* Generar método void "sumarConCero".
* Agregar anotación para Tests
* Agregar siguiente código

```
#!java

    @Test
	public void sumarConCero(){
		Calculadora calculadora = new Calculadora();
		assertEquals(3, calculadora.sumar(0, 3));
	}
```
* Corregir errores de compilación, crear nueva clase en src/main/java y crear método correspondiente. NO IMPLEMENTAR METODO!
* Nuestro primer ciclo de TDD!!
* Hacer pasar test "sumarConCero"
* Refactorizar y repetir ciclo


### Para continuar ###

* Escribir un test que valide la suma de 2 numeros cualquiera
* Escribir un test que valide la multiplicación por 0
* Escribir un test que valide la multiplicación por 1
* Escribir un test que valide la multiplicación entre 2 numeros cualquiera
* Escribir un test que valide la división por 1
* Escribir un test que valide la división exacta entre 2 numeros
* Escribir un test que valide la división por 0 (Chan! Que pasa aqui?)

### Para seguir :D ###
* Escribir una suite de tests para una "CalculadoraCientifica".
* Esta pobre calculadora cientifica sólo sabe aplicar potencias. Escribir una suite de tests para validar los casos de potencias
* OJO!!! La implementación de la potencia debe usar el mismo método de multiplicación expuesto en la primera calculadora

### Para la casa ###
 [Ejercicio original aquí. Revisalo despues de hacerlo tú mismo](http://technologyconversations.com/2013/12/20/test-driven-development-tdd-example-walkthrough/)

* Crear una sencilla calculadora de Strings con el metodo int Add(string numbers)
* El método puede recibir 0, 1 o 2 números, y retornará su suma (para un string vacío retornará 0) por ejemplo “” o “1” o “1,2”
* Permite que el método Add pueda manejar una cantidad indeterminada de números
* Permite que el método Add pueda manejar saltos de línea entre números (además de comas).
    El siguiente input está permitido: “1\n2,3” (resultado igual a 6)
* Perimite diferentes delimitadores
    Para cambiar un delimitador, el inicio del string contendrá una linea separada que se verá como esto: “//[delimitador]\n[numeros…]” por ejemplo “//;\n1;2” debería retornar 3 donde el delimitador por defecto es ‘;’ .
* La primera linea es opcional. Todos los escenarios existentes previamente deben ser soportados.
* Llamar al método Add con un numero negativo lanzara una excepción “negativos no permitidos” – y el negativo pasado al método. Si hay multiples negativos, mostrarlos todos en el mensaje de excepción

* ** Continúa si te crees capaz  **

* Los números mayores que 1000 deben ser ignorados, por lo que sumar 2 + 1001 dará como resultado 2
* Delimitadores pueden ser de cualquier largo con el siguiente formato: “//[delimitador]\n” por ejemplo: “//[—]\n1—2—3” debe retornar 6
* Permitir multiples delimitadores como: “//[delim1][delim2]\n” por ejemplo “//[-][%]\n1-2%3” debe retornar 6.
* Asegurate de permitir multiples delimitadores de largo mayor a 1 caracter


### Dudas? ###

* @jchacana (via twitter también)
* Follow up en sitio de [Meetup](http://www.meetup.com/es/Santiago-Devops-and-Continuous-Delivery-Meetup/events/224875298/)